# se genera una lista vacia luego a esta se añade una con elementos
def hacer_matriz():
    # se hace una lista vacía
    matriz = []
    # se agrega el elemento multiplicado 9 veces, para añadir 9
    for i in range(9):
        matriz.append(["[{0}]".format(i)]*9)
    return matriz

# se cambia la matriz en base a numeros para ser más facil de entender
def matriz_xy(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz)):
            matriz[i][j] = "[{0},{1}]".format(i, j)

# se encarga de imprimir la matriz el end = "" es para que el final no haga \n
def imprime(matriz):
    for i in range(9):
        for j in range(9):
            print(matriz[i][j], end="")
        print("")

def main():
    matriz = hacer_matriz()
    imprime(matriz)
    print("para ver la matriz más facilmente por coordenadas")
    matriz_xy(matriz)
    imprime(matriz)
    pass


if __name__ == '__main__':
    main()
